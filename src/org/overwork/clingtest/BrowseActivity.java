package org.overwork.clingtest;

import org.teleal.cling.android.AndroidUpnpService;
import org.teleal.cling.android.AndroidUpnpServiceImpl;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.LocalDevice;
import org.teleal.cling.model.meta.RemoteDevice;
import org.teleal.cling.registry.DefaultRegistryListener;
import org.teleal.cling.registry.Registry;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class BrowseActivity extends Activity implements
		AdapterView.OnItemClickListener {

	private AndroidUpnpService mUPnPService;

	private BrowseRegistryListener mRegistryListener = new BrowseRegistryListener();

	private ServiceConnection mServiceConnection = new ServiceConnectionImpl();

	private ListView mListView;

	private ArrayAdapter<DeviceDisplay> mAdapter;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_search && mUPnPService != null) {
			mUPnPService.getRegistry().removeAllRemoteDevices();
			mUPnPService.getControlPoint().search();
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if (parent == mListView) {
			Device<?, ?, ?> device = mAdapter.getItem(position).getDevice();
			String message = String.format("%s", device.toString());
			AlertDialog.Builder adb = new AlertDialog.Builder(this);
			adb.setMessage(message);
			adb.setNegativeButton(android.R.string.cancel, null);
			adb.show();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(mListView = new ListView(this));

		mAdapter = new ArrayAdapter<DeviceDisplay>(this,
				android.R.layout.simple_list_item_1);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(this);

		bindService(new Intent(this, AndroidUpnpServiceImpl.class),
				mServiceConnection, BIND_AUTO_CREATE);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mUPnPService != null) {
			mUPnPService.getRegistry().removeListener(mRegistryListener);
		}
		unbindService(mServiceConnection);
	}

	private class ServiceConnectionImpl implements ServiceConnection {

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			mUPnPService = (AndroidUpnpService) service;

			// Refresh the list with all known devices
			mAdapter.clear();
			for (Device<?, ?, ?> device : mUPnPService.getRegistry()
					.getDevices()) {
				mRegistryListener.deviceAdded(device);
			}

			// Getting ready for future device advertisements
			mUPnPService.getRegistry().addListener(mRegistryListener);

			// Search asynchronously for all devices
			mUPnPService.getControlPoint().search();
		}

		@Override
		public void onServiceDisconnected(ComponentName className) {
			mUPnPService = null;
		}
	}

	private class BrowseRegistryListener extends DefaultRegistryListener {

		@Override
		public void remoteDeviceDiscoveryStarted(Registry registry,
				RemoteDevice device) {
			deviceAdded(device);
		}

		@Override
		public void remoteDeviceDiscoveryFailed(Registry registry,
				final RemoteDevice device, final Exception ex) {
			runOnUiThread(new Runnable() {
				public void run() {
					Toast.makeText(
							BrowseActivity.this,
							"Discovery failed of '"
									+ device.getDisplayString()
									+ "': "
									+ (ex != null ? ex.toString()
											: "Couldn't retrieve device/service descriptors"),
							Toast.LENGTH_LONG).show();
				}
			});
			deviceRemoved(device);
		}

		@Override
		public void remoteDeviceAdded(Registry registry, RemoteDevice device) {
			// Log.w("remoteDeviceAdded", "Device: " + device);
			// RemoteService[] service = device.getServices(); //
			// 得到device的所有services
			// for (int n = 0; n < service.length; n++) {
			// Log.w("remoteDeviceAdded", "Service: " + service[n]);
			// Action[] getAction = service[n].getActions(); //
			// 得到其中servide的所有actions
			// for (int m = 0; m < getAction.length; m++) {
			// Action action = getAction[m];
			// Log.w("remoteDeviceAdded", "Action: " + action);
			//
			// ActionArgument[] aas = action.getArguments();
			// for (int a = 0; a < aas.length; a++) {
			// Log.w("remoteDeviceAdded", "Argument: " + aas[a]);
			// }
			// }
			// }

			deviceAdded(device);
		}

		@Override
		public void remoteDeviceRemoved(Registry registry, RemoteDevice device) {
			deviceRemoved(device);
		}

		@Override
		public void localDeviceAdded(Registry registry, LocalDevice device) {
			deviceAdded(device);
		}

		@Override
		public void localDeviceRemoved(Registry registry, LocalDevice device) {
			deviceRemoved(device);
		}

		public void deviceAdded(final Device<?, ?, ?> device) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					DeviceDisplay d = new DeviceDisplay(device);
					int position = mAdapter.getPosition(d);
					if (position >= 0) {
						// Device already in the list, re-set new value at same
						// position
						mAdapter.remove(d);
						mAdapter.insert(d, position);
					} else {
						mAdapter.add(d);
					}
				}
			});
		}

		public void deviceRemoved(final Device<?, ?, ?> device) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					mAdapter.remove(new DeviceDisplay(device));
				}
			});
		}
	}

	public static class DeviceDisplay {

		private Device<?, ?, ?> mDevice;

		public DeviceDisplay(Device<?, ?, ?> device) {
			this.mDevice = device;
		}

		public Device<?, ?, ?> getDevice() {
			return mDevice;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (o == null || getClass() != o.getClass())
				return false;
			DeviceDisplay that = (DeviceDisplay) o;
			return mDevice.equals(that.mDevice);
		}

		@Override
		public int hashCode() {
			return mDevice.hashCode();
		}

		@Override
		public String toString() {
			// Display a little star while the device is being loaded
			return mDevice.isFullyHydrated() ? mDevice.getDisplayString()
					: mDevice.getDisplayString() + " *";
		}
	}
}
